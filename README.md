# Atlassian SDK docker image

This Atlassian SDK version contains yarn to build WRMPlugin-based plugins.

To build and to publish:

```
docker build -t babinvn/atlassian-sdk .
docker login -u babinvn -p <password>
docker push babinvn/atlassian-sdk
```
